var app = angular.module("quantumConnect", ['ui.router', 'oc.lazyLoad']);
app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
    //  $httpProvider.interceptors.push('requestInterceptor');
    //    $httpProvider.defaults.headers.common = {};
    //    $httpProvider.defaults.headers.post = {};
    //    $httpProvider.defaults.headers.put = {};
    //    $httpProvider.defaults.headers.patch = {};
    //    $httpProvider.defaults.withCredentials = true;

    // $stateProvider.state("dashbord", {
    //     url: '/dashbord',
    //     templateUrl: '../templates/dashboard.html',
    //     controller: 'dashbord',
    //     resolve: {
    //         home: function($ocLazyLoad) {
    //             return $ocLazyLoad.load({
    //                 name: "dashbord",
    //                 files: ["/js/angular/controllers/dashbord.js", "/js/angular/services/connection.js", "/js/angular/factory/interceptor.js"]
    //             })
    //         }

    //     }
    // }).state("login", {
    //     views: {
    //         'header': {
    //             template: '<div>header......{{name}}.<button ng-click="test()">sdsd</button></header>',
    //         },
    //         'login': {
    //             url: '/login',
    //             templateUrl: '../templates/login.html',
    //             controller: 'login',
    //             resolve: {
    //                 home: function($ocLazyLoad) {
    //                     return $ocLazyLoad.load({
    //                         name: "login",
    //                         files: ["/js/angular/controllers/login.js", "/js/angular/services/connection.js", "/js/angular/factory/interceptor.js"]
    //                     })
    //                 }

    //             }
    //         }
    //     }
    // })
    $stateProvider.state('myapp', {
        views: {
          'header': {
            templateUrl:'../templates/header.html',
            controller:'header'
          },
          'content': {
              url:'/',
            template:'<div ui-view></div>',
          },'sidemenu': {
            templateUrl:'../templates/sidemenu.html'
          }
        }
      })
      .state('myapp.login', {
        url:'/admin',
        templateUrl:'../templates/login.html',
        controller:'login',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/login.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.dashbord', {
        url:'/dashbord',
        templateUrl:'../templates/dashboard.html',
        controller:'dashbord',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/dashbord.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.signup', {
        url:'/signup',
        templateUrl:'../templates/signup.html',
        controller:'signup',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/signup.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.notification', {
        url:'/notification',
        templateUrl:'../templates/notifications.html',
        controller:'notification',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/notification.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.invite', {
        url:'/invite',
        templateUrl:'../templates/invites.html',
        controller:'invite',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/invite.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.payDue', {
        url:'/payDue',
        templateUrl:'../templates/paymy-dues.html',
        controller:'payDue',
        params:{
            paybill:null
        },
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/payDue.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.book', {
        url:'/book',
        templateUrl:'../templates/book-facilities.html',
        controller:'book',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/fullcalendar/fullcalendar.min.js","/js/fullcalendar/fullcalendar.css","/js/angular/controllers/book.js","/js/angular/services/connection.js","/js/angular/services/util.js",,"js/fullcalendar/demo.js"]
                })
            }

        }
      }).state('myapp.offers', {
        url:'/offers',
        templateUrl:'../templates/offers-discounts.html',
        controller:'offers',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/offers.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.emergency', {
        url:'/emergency',
        templateUrl:'../templates/emergency-contact.html',
        controller:'emergency',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/emergency.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.complaints', {
        url:'/complaints',
        templateUrl:'../templates/complaint-service-request.html',
        controller:'complaints',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/complaintService.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.feedback', {
        url:'/feedback',
        templateUrl:'../templates/service-feedback.html',
        controller:'feedback',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/feedback.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      }).state('myapp.reference', {
        url:'/reference',
        templateUrl:'../templates/reference.html',
        controller:'reference',
        reload: true,
        inherit: false,
        notify: true,
        resolve: {
            home: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: "test",
                    files: ["/js/angular/controllers/reference.js","/js/angular/services/connection.js","/js/angular/services/util.js"]
                })
            }

        }
      })
  $urlRouterProvider.otherwise('/admin');
//    /$locationProvider.html5Mode(true);

}]).run(function($http) {
    //console.log("===>");

  //  $state.go('myapp.login');
  }).controller('header',function($scope,$state,$rootScope,util,connection) {
//      $scope.logout = function() {
//     $state.go('myapp.login')
//    }
   $rootScope.fetchUser = function() {
    request = {
        body: {

        },
        params : util.setGet('userFetch','?userId=' + $rootScope.username)
    };
    var promise1 = connection.sendRequest(request);
    promise1.then( function(response) {
        localStorage.setItem('unameFirst',response.data[0].firstname)
        $rootScope.unameFirst = response.data[0].firstname;
    }, function(err){
        $rootScope.unameFirst = 'admin';
    })
    }


if(localStorage.getItem('username'))
{
    $rootScope.username = localStorage.getItem('username');
    $rootScope.fetchUser();
}


$scope.logout = function(){
    request = {
        body: {},
        params : util.setPost('logout')
    };
    var logoutpromise = connection.sendRequest(request);
    logoutpromise.then( function(response) {
        localStorage.clear();
        $state.go('myapp.login')
    }, function(err){
    })


}
  })
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('HttpResponseErrorHandler');
}]);
  app.factory('HttpResponseErrorHandler', ['$q', '$rootScope', '$timeout','$state', function($q, $rootScope, $timeout,$state) {
    return {
        response: function(response) {
          //  console.log(response.status);

            return response;
        },
        responseError: function(rejection) {
            console.log('rejected: ' + rejection.status);

            if(rejection.status===401) {
             $state.go('myapp.login');
            confirm('session expires.. Please Login')
            } 
            else
            return $q.reject(rejection);
        }
    };
}]);