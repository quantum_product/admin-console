// angular.module("quantumConnect").service('connection', function ()  {
// this.getData = function()  {
//     return "testing data";
// }
// })
angular.module('quantumConnect').factory('connection', ['$rootScope', '$http', function($rootScope, $http) {
    var connection = {}
    if(!$http.defaults.headers.common.Authorization) {
	    $http.defaults.headers.common.Authorization = localStorage.getItem('cred');
         }

    // connection.prototype.platformFinder = function(key, req) {
    //     var url;
    //     var releaseMode = $rootScope.configObject.app['releaseMode'];
    //     var connectionUrl = $rootScope.configObject.platform[releaseMode].host
    //     url = connectionUrl + $rootScope.configObject.requests[req.echo.requestOwner];
    //     return url;
    // }
//https  https://dev-ws.quantumconnect.in
    connection.sendRequest = function(request) {

      var req =  {
            
                "method": request.params.method,
                "url":"https://nexterra-admin.quantumconnect.in" + request.params.url,
                "header": 
                    {
                       "Content-Type": "application/json",
                        "Authorization": localStorage.getItem('cred')//"6b75889d-69af-8faa-c268-480d01488d38"
                    },
                "data": JSON.stringify(request.body)
        }


        // var req = {
        //     method: 'GET',
        //     // url: this.platformFinder('pilot', req),
        //     url: '../../../config/data.json',
        //     headers: {
        //         'content-type': 'application/json'
        //     },
        //     timeout: 60000,
        //     crossDomain: true,
        //     withCredentials: true,
        //     data: ''//JSON.stringify(req)
        // }

        // $http(req).then(function(response) {
        //  //   scope.processResponse(response);
        // }, function(err) {

        // });
     // console.log(req)
return $http(req);
    }
    return connection;
}]);