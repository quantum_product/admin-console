angular.module("quantumConnect").service('util', function ($http)  {
    var requestConfig = {
        login: '/users/login',
        invite: '/users/invite',
        signup:'/users/signup',
        fetchUser: '/users/fetch/',
        sample: '/1',
        sample2: '/4',
        fetchAllInvite: '/invite/all',
        createInvite: '/invite/create',
        passInvite: '/invite/pass',
        updateInvite: '/invite/update/',
        updateInviteStatus: '/invite/status/',
        inviteForMe: '/invite/for-me',
        inviteByMe: '/invite/by-me',
        generateOtp: '/otp/generate/',
        validateOtp: '/otp/validate',
        updateProfile: '/users/profile/',
        changePassword: '/users/password/change/',
        updateUserRole: '/users/role/update',
        enableUser: '/users/enable',
        disableUser: '/users/disable',
        addBanner: '/banner/add',
        allBanner: '/banner/all',
        userFetch: '/users/fetch',
        masterCount: '/master/users/count',
        entityCount: '/master/users/count?entity=casa',
        countIn: '/master/users/count/login',
        countOut: '/master/users/count/logout',
        activeUser: '/master/users/count/active',
        payDue: '/payment/due/all',
        createDue: '/payment/due/create',
        logout: '/users/logout',
        listAllBook: '/bookings/all',
        BookingCreate: '/bookings/create',
        emergencyCreate:'/emergency/create',
        emergencyType:'/emergency/by/',
        emergencyUpdate:'/emergency/update',
        emergencyGetAll: 'emergency/all',
        getAllOffer:'/offers/all',
        createOffer:'/offers/create',
        serviceRequest: '/service_request/all',
        feedback: '/service_response/all',
        survey:'/service_response/getAllSurvey',
        reference:'/referral/all'



    }
    this.setPost = (val, addParams) => {
        addParams =  addParams ? addParams : '';
        var postReq = {
            url : requestConfig[val] + addParams,
            method : 'POST'
        }
        return postReq;
    }
    this.setGet = (val, addParams) => {
        addParams =  addParams ? addParams : '';
        var getReq = {
            url : requestConfig[val] + addParams,
            method : 'GET'
        }
        return getReq;
    }
    this.setPut = (val, addParams) => {
        addParams =  addParams ? addParams : '';
        var getReq = {
            url : requestConfig[val] + addParams,
            method : 'PUT'
        }
        return getReq;
    }
    this.showLoader = () =>{
            $('#loader').show();
    }
    this.hideLoader = () =>{
            $('#loader').hide();
    }
    this.setToken = (token) => {
        $http.defaults.headers.common.Authorization = token;

    }

});
angular.module("quantumConnect").service('PagerService', function ()  {
        // service definition
        var service = {};

        service.GetPager = GetPager;

        return service;

        // service implementation
        function GetPager(totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;

            // default page size is 10
            pageSize = pageSize || 10;

            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            var pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }
});

