angular.module("quantumConnect").controller('invite', function($scope, $rootScope, connection, $http, $q, $state, util) {
    $scope.fetchInvite = {
        body: {},
        params: util.setGet('fetchAllInvite')
    };
    util.showLoader();
    connection.sendRequest($scope.fetchInvite).then((response) => {
        util.hideLoader();
        $scope.servicesValues = response.data;


        //           [
        //            {
        //                'name':'Party Hall',
        //                'mobile':9876543210,
        //                'purpose':'Bank',
        //                'status':'INVITED',
        //                'otp_secret':5875
        //            },{
        //                'name':'pool',
        //                'mobile':9876543210,
        //                'purpose':'Bank',
        //                'status':'INVITED',
        //                'otp_secret':2121
        //            },{
        //                'name':'parking',
        //                'mobile':9876543210,
        //                'purpose':'Bank',
        //                'status':'INVITED',
        //                'otp_secret':4444
        //            },{
        //                'name':'Party Hall',
        //                'mobile':9876543210,
        //                'purpose':'Bank',
        //                'status':'in',
        //                'otp_secret':5875
        //            },{
        //                'name':'Double',
        //                'mobile':9876543210,
        //                'purpose':'Bank',
        //                'status':'out',
        //                'otp_secret':2222
        //            }
        //            ];
    }, (err) => {
        util.hideLoader();
        confirm("please login");
        $state.go('myapp.login');
    })
    $scope.cancelInvite = function(){
        
    }
    //    $scope.servicesValues = [
    //    {
    //        "id": 1,
    //        "user_id": null,
    //        "userhash": null,
    //        "name": "George",
    //        "mobile": "9952431979",
    //        "relationship": "brother",
    //        "invited_by": "b423f70a-8cf6-7edd-a761-2880e7d16f20",
    //        "purpose": "family function",
    //        "status": "INVITED",
    //        "visit_date": "2018-05-23T00:00:00.000Z",
    //        "visit_time": 600,
    //        "created_at": "2018-06-05T04:18:58.000Z",
    //        "updated_at": "2018-06-05T04:18:58.000Z",
    //        "id_proof_type": null,
    //        "otp_secret": null
    //    },
    //    {
    //        "id": 2,
    //        "user_id": null,
    //        "userhash": null,
    //        "name": "George",
    //        "mobile": "9952431979",
    //        "relationship": "brother",
    //        "invited_by": "480d4771-acff-1695-d3c5-197aa9d2f4ba",
    //        "purpose": "family function",
    //        "status": "INVITED",
    //        "visit_date": "2018-05-23T00:00:00.000Z",
    //        "visit_time": 600,
    //        "created_at": "2018-06-06T17:07:26.000Z",
    //        "updated_at": "2018-06-06T17:07:26.000Z",
    //        "id_proof_type": null,
    //        "otp_secret": null
    //    }
    //];


});