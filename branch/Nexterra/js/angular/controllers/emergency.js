angular.module("quantumConnect").controller('emergency', function ($scope, $rootScope, connection, $http, $q, $state, util) {
    $scope.emergencyContacts;
    $scope.addEmergencyContact = function () {
        if ($scope.update === 'ADD') {
            var request = {
                body: {
                    emergency_type: $scope.emergencyType,
                    contact: $scope.emergencyContact
                },
                params: util.setPost('emergencyCreate')
                //params: util.setPut('emergencyCreate')

            }
            connection.sendRequest(request).then(function (response) {
                // $scope.emergencyContacts = response.data;
                
                $scope.init();
            }, function (err) {

            })
        }
        $scope.emergencyType = '';
        $scope.emergencyContact = '';

    }
    $scope.updatingContacts = function (type, contact, operation, id) {
        $scope.emergencyContact = contact;
        $scope.emergencyType = type;
        $scope.update = operation;
        $scope.idtoUpdate = id;
    }
    $scope.editDelete = function(operation) {
    
        if(operation=='DELETE')
        $scope.emergencyContact = '';
        var request = {
            body: {
                emergency_type: $scope.emergencyType,
                contact: $scope.emergencyContact
            },
            params: util.setPut('emergencyUpdate', '/' + $scope.idtoUpdate)
            //params: util.setPut('emergencyCreate')

        }
        connection.sendRequest(request).then(function (response) {
            // $scope.emergencyContacts = response.data;
            $scope.init();
        }, function (err) {
            $scope.init();
        })
        $scope.emergencyType = '';
        $scope.emergencyContact = '';
        $scope.idtoUpdate = '';

    }
    $scope.init = function () {
        // var request = {

        //     params: util.setGet('emergencyGetAll')
        //     //params: util.setGet('emergency/all')
        // }
        // connection.sendRequest(request).then( function (response) {
        // //if($scope.emergencyContacts.length)
        //  $scope.emergencyContacts = response.data;
        //  console.log($scope.emergencyContacts)
        // }, function (err) {

        // })
        $http({
            url: 'http://13.232.65.236:3000/emergency/all',
            method: 'GET'
        }).then(function (response) {
            $scope.emergencyContacts = response.data;
        }, function () {
            //alert('fail')
        })
    }
    $scope.init();
});