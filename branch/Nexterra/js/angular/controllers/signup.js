angular.module("quantumConnect").controller('signup', function($scope,$rootScope,connection,$http,$state,util) {
  $(document).ready( () => {
    //  if(location.href.indexOf('login'))
    //  {
       $('#header,#nav').hide();

     //}
   })
   $scope.signupCall = function () {
     var signupPromise = {
       body: {
         "email": $scope.email,
           "mobile": $scope.mobile.toString(),
           "password": $scope.password,
           "sex":$scope.sex,
           "firstname": $scope.firstName,
           "lastname": $scope.lastName,
           "birth": $scope.dob,
           "device":
           {
             "id": "somedeviceid",
             "type": "Desktop",
             "model": "chrome"
           },
          "address":
           {
             "line1":$scope.addrLine1,
             "line2": $scope.addrLine2,
             "line3": $scope.addrLine3,
             "city" : $scope.addrCity,
             "state": $scope.addrState,
             "pincode":$scope.addrPincode
           }

       },
       params: util.setPost('signup')
     }
     connection.sendRequest(signupPromise).then( function (response) {
     // console.log(response);
      $state.go('myapp.login');
     }, function (error) {
     // console.log(error)
     }  )
   }
});
