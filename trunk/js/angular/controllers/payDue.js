angular.module("quantumConnect").controller('payDue',function($scope,$state,$rootScope,util,connection,$http) {
 $rootScope.ctrlname = 'payDue';
 $scope.userType = ['select service','Plumbing', 'Electrical'];
 $scope.userTypeamount = ['', 250, 150];
 $scope.taxpercentage = 15;
 if(localStorage.getItem('username'))
{
    $rootScope.username = localStorage.getItem('username');  
}  
   $scope.httpObj = function(req) {
    var request =  {
            
        "method": req.params.method,
       // "url":"https://dev-ws.quantumconnect.in" + req.params.url,
       // "url":"http://13.232.65.236/payments/payment/due/all",
        "url":"http://13.232.65.236/payments"+ req.params.url,
        "header": 
            {
               "Content-Type": "application/json",
                "Authorization": localStorage.getItem('cred')
            },
        "data": JSON.stringify(req.body)
    }
    return request;
}
$scope.setAmount = function (val) {
    var index = $scope.userType.indexOf(val);
    if(index)
    $scope.amount = $scope.userTypeamount[index];
}
    $scope.init = function() {
        var  paybillList = { 
            body: {},
            params : util.setGet('payDue',)
        };
        var req = $scope.httpObj(paybillList);
        $http(req).then( function(response) {
          var arrlist  = response.data;
            _.each(arrlist,function(val){
                if(val.status=='PASS_GENERATED')
                val.status='PAID';
                
                });
                $scope.paydueList = arrlist;
            // alert(JSON.stringify(response))
        }, function(err) {
  
          //  $state.go('myapp.login')
            // alert(JSON.stringify(err))
        })
    }

    $scope.init();
    $scope.print = function(val) {
        $scope.invoice = val;
        setTimeout( function(){
            window.print();
        },300)

    }
    $scope.email = function(val) {
        $scope.invoice = val;
        setTimeout( function() {
            window.print();
        },300) 
        
    }
    $scope.setTotal = function() {
        if($scope.gst && $scope.taxpercentage)
        $scope.totalamount = $scope.amount + (($scope.amount/100)*$scope.taxpercentage);
        else
        $scope.totalamount = $scope.amount;
    }
    $scope.createDue = function() {
        var  createdue = { 
            body: {
                "payer": $scope.phone,
                "amount": $scope.totalamount,
                "receiver": $scope.vendor_type,
                "dueDate": $scope.dueDate
            },
            params : util.setPost('createDue')
        };
        var req = $scope.httpObj(createdue);
       $http(req).then( function(response) {
           $scope.paydueList = response.data;
           document.getElementById('paymydues').reset();
         $scope.init();
        }, function(err) {
          
            document.getElementById('paymydues').reset();
           // $state.go('myapp.login')
        })
    }
if($state.params.paybill) {
    $scope.params = $state.params.paybill;
    $scope.userType.unshift($scope.params.service_type);
}

})