
angular.module('quantumConnect', []).filter('companyFilter', [function () {
    return function (clients, selectedCompany) {
        if (!angular.isUndefined(clients) && !angular.isUndefined(selectedCompany) && selectedCompany.length > 0) {
            var tempClients = [];
            angular.forEach(selectedCompany, function (id) {
                angular.forEach(clients, function (client) {
                    if (angular.equals(client.company.id, id)) {
                        tempClients.push(client);
                    }
                });
            });
            return tempClients;
        } else {
            return clients;
        }
    };
}]);
angular.module("quantumConnect").controller('feedback', function($scope, $rootScope, connection, $http, $q, $state, util, $filter, PagerService) {
    var sortingOrder = 'name';
   

        $scope.init = function () {
            // $scope.items = [
            //     {"date":"05-07-2018","name":"kkkk","description":"description 1","email":"kkk@gmail.com","rating":"3","mobile ":"989898989","phone":"989898989",            "receiver": "TT Court",
            // }, 
            //     {"date":"05-07-2018","name":"yyyy","description":"description 1","email":"yyy@gmail.com","rating":"5","mobile ":"889898989","phone":"889898989",            "receiver": "TT Court",
            // }, 
            //     {"date":"05-07-2018","name":"zzz","description":"description 1","email":"zzz@gmail.com","rating":"4","mobile ":"789898989","phone":"789898989",            "receiver": "Maintenance",
            // }, 
            //     {"date":"25-07-2018","name":"ggg","description":"description 1","email":"ggg@gmail.com","rating":"1","mobile ":"489898989","phone":"489898989",            "receiver": "Maintenance",
            // }, 
            //     {"date":"15-07-2018","name":"xxx","description":"description 1","email":"alwink@gmail.com","rating":"2","mobile ":"689898989","phone":"689898989",            "receiver": "Maintenance",
            // },
            //     {"date":"15-07-2018","name":"xxx","description":"description 1","email":"alwink@gmail.com","rating":"3","mobile ":"689898989","phone":"689898989",            "receiver": "TT Court",
            // }
                
            //     ];
           var request = { 
                body: {
                    
                },
                params : util.setGet('feedback')
            };
            var feedback = connection.sendRequest(request);
            feedback.then( function(response) {
                $scope.items = response.data;
                  // functions have been describe process the data for display
                $scope.search();

             });
             var request2 = {
                 body: {},
                 params: util.setGet('survey')
             };
             var survey = connection.sendRequest(request2);
             survey.then( function(response) {
                $scope.items1 = response.data;
                  // functions have been describe process the data for display
              //  $scope.search2();

             });
           
        }
        $scope.init();


    $scope.sortingOrder = sortingOrder;
    $scope.reverse = false;
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 6;
    $scope.pagedItems = [];
    $scope.currentPage = 0;

   

    var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.items, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        // take care of the sorting order
        if ($scope.sortingOrder !== '') {
            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
        }
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };
    
    // calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
    };
    
    $scope.range = function (start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
            $scope.currentPage++;
        }
    };
    
    $scope.setPage = function () {
        $scope.currentPage = this.n;
    };

  
    // change sorting order
    $scope.sort_by = function(newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

        // icon setup
        $('th i').each(function(){
            // icon reset
            $(this).removeClass().addClass('icon-sort');
        });

    };


    });