angular.module("quantumConnect").controller('reference', function($scope,$rootScope,connection,util) {

    $scope.init =function(){
        var request = { 
            body: {},
            params : util.setGet('reference')
        };
        connection.sendRequest(request).then(function (response) {
            $scope.refernceList = response.data;
        });
    }
    $scope.init();
});