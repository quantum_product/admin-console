angular.module("quantumConnect").controller('book', function($scope,$rootScope,connection,$http,$q,$state,util) {
    $scope.userType = ['select vendor','Maintenance', 'Party Hall']

    $scope.init = function () {
     var request = {
        body: {},
        params: util.setGet('listAllBook')
     }
     $scope.today = new Date();
     connection.sendRequest(request).then( function(response) {
        //console.log(JSON.stringify(response))
        var arr = response.data;
        $scope.bookedList = response.data;
        var arr1 = [];
        arr = arr.map(function(val,ind,array) { 
          var obj = {};
          obj.start = new Date(val.start_datetime).toLocaleDateString('en-US');
          obj.end = new Date(val.end_datetime).toLocaleDateString('en-US'); 
          obj.title = val.purpose;
          obj.id = val.id;
          obj.startTime = new Date(val.start_datetime).toLocaleTimeString();
          obj.endTime = new Date(val.end_datetime).toLocaleTimeString();
          obj.name = (val.user.firstname ? val.user.firstname : '') + " " + (val.user.lastname ? val.user.lastname : '') ;
          obj.mobile = val.user.mobile;
          obj.allDay = Math.round((val.start_datetime-val.end_datetime)/(1000*60*60*24)) > 0 ? true : false; 
              arr1.push(obj)
            })
            $('.calendar').empty();
            $(function() {

              $('#calendar').fullCalendar({
                eventClick: function(eventObj) {
                  if (eventObj.url) {
                    alert(
                      'Booked For: ' +eventObj.title + '.\n' +
                      'Booked by: ' + eventObj.name + 
                      '.\n' +
                      'Phone no: ' + eventObj.mobile +
                      '.\n' +
                      'Date and Time: ' + (eventObj.startTime + eventObj.endTime) +
                      '.\n' +
                      'Refernce Id ' + eventObj.id 
                    );
            
            
                    return false; // prevents browser from following link in current tab.
                  } else {
                    alert(
                      'Booked For: ' +eventObj.title + '.\n' +
                      'Booked by: ' + eventObj.name + 
                      '.\n' +
                      'Phone no: ' + eventObj.mobile +
                      '.\n' +
                      'Date and Time : ' + (new Date(eventObj.start).toLocaleDateString('en-US')+" "+ eventObj.startTime +" to "+new Date(eventObj.end).toLocaleDateString('en-US')+" "+ eventObj.endTime) +
                      '.\n' +
                      'Refernce Id: ' + eventObj.id 
                    );
                    //alert('Clicked ' + eventObj.title);
                  }
                },
                defaultDate: '2018-07-07',
                events: arr1
              });
            
            });
    //         !function ($) {
    //           var eventObject = {};
    //             $(function(){
              
    //               // fullcalendar
    //               var date = new Date();
    //               var d = date.getDate();
    //               var m = date.getMonth();
    //               var y = date.getFullYear();
    //               var addDragEvent = function($this){
    //                 // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
    //                 // it doesn't need to have a start or end
    //                 eventObject = {
    //                   title: $.trim($this.text()), // use the element's text as the event title
    //                   className: $this.attr('class').replace('label','')
    //                 };
                    
    //                 // store the Event Object in the DOM element so we can get to it later
    //                 $this.data('eventObject', eventObject);
                    
    //                 // make the event draggable using jQuery UI
    //                 $this.draggable({
    //                   zIndex: 999,
    //                   revert: true,      // will cause the event to go back to its
    //                   revertDuration: 0  //  original position after the drag
    //                 });
    //               };
                  
    //               $('.calendar').each(function() {
    //                 $(this).fullCalendar({
    //                   header: {
    //                     left: 'prev',
    //                     center: 'title',
    //                     right: 'next'
    //                   },
    //                   editable: true,
    //                   droppable: true, // this allows things to be dropped onto the calendar !!!
    //                   drop: function(date, allDay) { // this function is called when something is dropped
                        
    //                       // retrieve the dropped element's stored Event Object
    //                       var originalEventObject = $(this).data('eventObject');
                          
    //                       // we need to copy it, so that multiple events don't have a reference to the same object
    //                       var copiedEventObject = $.extend({}, originalEventObject);
                          
    //                       // assign it the date that was reported
    //                       copiedEventObject.start = date;
    //                       copiedEventObject.allDay = allDay;
                          
    //                       // render the event on the calendar
    //                       // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
    //                       $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                          
    //                       // is the "remove after drop" checkbox checked?
    //                       if ($('#drop-remove').is(':checked')) {
    //                         // if so, remove the element from the "Draggable Events" list
    //                         $(this).remove();
    //                       }
                          
    //                     }
    //                   ,
    //                   events: arr
    //                 });
    //               });
                  
    //               $('#myEvents').on('change', function(e, item){
    //                 addDragEvent($(item));
    //               });
              
    //               $('#calendar li').each(function() {
    //                 addDragEvent($(this));
    //                 alert();
    //               });
              
    //               $(document).on('click', '#dayview', function() {
    //                 $('.calendar').fullCalendar('changeView', 'agendaDay')
    //                 alert(1);
    //               });
              
    //               $('#weekview').on('click', function() {
    //                 $('.calendar').fullCalendar('changeView', 'agendaWeek');
    //                 alert(2);
    //               });
              
    //               $('#monthview').on('click', function() {
    //                 $('.calendar').fullCalendar('changeView', 'month');
    //                 alert(3);
    //               });
              
    //             });
    //           }(window.jQuery);
    //  }, function(err){

     })

    };

    $scope.createBooking = function () {
        if($scope.startDate <= $scope.endDate) {
        var request =  {
            body: {
                facility_type :$scope.vendor_type,
                start_datetime :$scope.startDate.toISOString().slice(0,10),
                end_datetime:$scope.endDate.toISOString().slice(0,10),
                purpose :$scope.purpose
            },
            params: util.setPost('BookingCreate')
         }
         connection.sendRequest(request).then(function () {
        $scope.init();
        document.getElementById('createBookingForm').reset();
        }, function () {
         // alert('fail')
         })
        } else {
            confirm('please enter proper start date and end date')
        }
    }
    
    
    $scope.init();
    
});