angular.module("quantumConnect").controller('dashbord', function($scope,$rootScope,connection,$http,$q,$state,util) {
    $scope.defer = $q.defer();
   $('#nav,#header').show();

   // console.log(connection.getData())
  $scope.sendRequest = () => {

  }

  $scope.init = () => {
    $rootScope.showHeader = true;
    $scope.dataSize = 180;
    // $scope.totalUser = 3450;
    // $scope.activeUser = 3450;
    // $scope.visitorCheckin = 3450;
    // $scope.visitorCheckout = 3450;
  
  }
  $rootScope.redirect = (val) => {
 
  }
  $scope.init();
//   $scope.re1 = connection.sendRequest(requestBody,reqParams);
//   $scope.re2 = connection.sendRequest(requestBody,reqParams);
request = { 
    body: {
        // "email": "arther.tony02@gmail.com",
        // "mobile": "9500204064",
        // "password": "123456789",
        // "sex": "M",
        // "firstname": "Arther",
        // "lastname": "Gunaseelan",
        // "birth": "23-05-1989",
        // "device": {
        //     "id": "somedeviceid",
        //     "type": "somedevicetype",
        //     "model": "IOS"
        // },
        // "address": {
        //     "line1": "Flat 20",
        //     "line2": "Radian Mandrine, Thuraipakkam",
        //     "line3": "Opp. To. Jain College",
        //     "city": "Chennai",
        //     "state": "TamilNadu",
        //     "pincode": "600001"
        // }
    },
    params : util.setGet('userFetch','?userId=' + $rootScope.username)
};

var promise1 = connection.sendRequest(request);

request2 = { 
    body: {
        
    },
    params : util.setGet('masterCount')
};
var masterCount = connection.sendRequest(request2);
masterCount.then( function(response) { 
    $scope.dataSize = 180;
    $scope.totalUserPercent = response.data.user_count;
    $scope.totalUser = response.data.user_count;
    $('.chart1').attr('data-percent',$scope.totalUser)
            $('.chart1').easyPieChart({barColor: '#ef1e25',trackColor: '#f2f2f2',scaleColor: '#dfe0e0',lineCap: 'round',lineWidth: 3,size: 110,animate: 1000,onStart: $.noop,onStop: $.noop}); 


}, function(err){
    $scope.errorhandler(err);

})

request3 = { 
    body: {
        
    },
    params : util.setGet('entityCount')
};
var entityCount = connection.sendRequest(request3);
entityCount.then( function(response) { 
    //console.log(JSON.stringify(response))
    $scope.entityUserCount = response.data.user_count;
    $scope.entityUserPercent = parseInt(response.data.user_count)>100?100:parseInt(response.data.user_count);
    $('.chart2').attr('data-percent', $scope.entityUserPercent)

        $('.chart2').easyPieChart({barColor: '#ef1e25',trackColor: '#f2f2f2',scaleColor: '#dfe0e0',lineCap: 'round',lineWidth: 3,size: 110,animate: 1000,onStart: $.noop,onStop: $.noop}); 

}, function(err){
    $scope.errorhandler(err);

//console.log('fail');
})

request4 = { 
    body: {
        
    },
    params : util.setGet('countIn')
};
var countIn = connection.sendRequest(request4);
countIn.then( function(response) { 
   // console.log(JSON.stringify(response));
    $scope.checkedInCount = response.data.user_count;
    $scope.checkedInPercent = parseInt(response.data.user_count)>100?100:parseInt(response.data.user_count);
    $('.chart3').attr('data-percent',$scope.checkedInPercent)
    $('.chart3').easyPieChart({barColor: '#ef1e25',trackColor: '#f2f2f2',scaleColor: '#dfe0e0',lineCap: 'round',lineWidth: 3,size: 110,animate: 1000,onStart: $.noop,onStop: $.noop}); 

}, function(err) {
    $scope.errorhandler(err);

//console.log('fail');
})

request5 = { 
    body: {
        
    },
    params : util.setGet('activeUser')
};
var activity = connection.sendRequest(request5);
activity.then( function(response) { 
    //console.log(JSON.stringify(response));
    $scope.activeCount = response.data.user_count;
    $scope.activePercent = parseInt(response.data.user_count)>100?100:parseInt(response.data.user_count);
    $('.chart4').attr('data-percent',$scope.activePercent)
  $('.chart4').easyPieChart({barColor: '#ef1e25',trackColor: '#f2f2f2',scaleColor: '#dfe0e0',lineCap: 'round',lineWidth: 3,size: 110,animate: 1000,onStart: $.noop,onStop: $.noop}); 
}, function(err) {
$scope.errorhandler(err);
//console.log('fail');
});
$scope.errorhandler = function(error) {
    if(error.status === 401 || error.status ===403)
    {
        $state.go('myapp.login')
    }
}
$rootScope.fetchUser();


//   $scope.dataPercent = 20;
//   $scope.dataSize = 180;
//   setTimeout(function() {
//    $('.easypiechart').easyPieChart({
//     // The color of the curcular bar. You can pass either a css valid color string like rgb, rgba hex or string colors. But you can also pass a function that accepts the current percentage as a value to return a dynamically generated color.
//     barColor: '#ef1e25',
//     // The color of the track for the bar, false to disable rendering.
//     trackColor: '#f2f2f2',
//     // The color of the scale lines, false to disable rendering.
//     scaleColor: '#dfe0e0',
//     // Defines how the ending of the bar line looks like. Possible values are: butt, round and square.
//     lineCap: 'round',
//     // Width of the bar line in px.
//     lineWidth: 3,
//     // Size of the pie chart in px. It will always be a square.
//     size: 110,
//     // Time in milliseconds for a eased animation of the bar growing, or false to deactivate.
//     animate: 1000,
//     // Callback function that is called at the start of any animation (only if animate is not false).
//     onStart: $.noop,
//     // Callback function that is called at the end of any animation (only if animate is not false).
//     onStop: $.noop
//   });   
//   })
     

})

