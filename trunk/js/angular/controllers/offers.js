angular.module("quantumConnect").controller('offers', function($scope,$rootScope,connection,$http,$q,$state,util) {


$scope.offerType = ['CASHBACK','DIWALI','DISCOUNT']

  //  http://13.232.65.236:3000/offers/all

    $scope.init = function (){
        var request = {
            body:{},
            params : util.setGet('getAllOffer')
        }
        connection.sendRequest(request).then( function (response) {
        $scope.offerList = response.data;
     //   console.log($scope.offerList)
        }, function (err) {

        })

    };
    $scope.createOffer = function () {
        if($scope.startDate <= $scope.endDate) {
        var request = {
            body:{
                "offer_name": $scope.name,
                "offer_type": $scope.offer_type,
                "offer_amount": $scope.amount,
                "offer_description": $scope.desc,
                "start_datetime": $scope.startDate.toISOString().slice(0,10),
                "end_datetime": $scope.endDate.toISOString().slice(0,10)
            },
            params : util.setPost('createOffer')
        }

        connection.sendRequest(request).then( function (response) {
         $scope.init();
        }, function (err) {

        })   
    } else {
        confirm('Please enter proper start and end date');
    }
        
    }
    $scope.init();

});