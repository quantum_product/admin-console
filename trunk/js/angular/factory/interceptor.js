  angular.module("quantumConnect").factory('requestInterceptor', function (util)  {
    var httpinterceptor =  {
        request: function(config) {
          util.showLoader();

        //    console.log(config)
          return config;
        },
    
        requestError: function(config) {
          util.hideLoader();

          console.log(config)
          return config;
        },
    
        response: function(res) {
          util.hideLoader();

          //console.log(res)
          return res;
        },
    
        responseError: function(res) {
          util.hideLoader();

         // console.log(res)
          return res;
        }
      }
      return httpinterceptor;
    })